# Git Flip

Flip from one branch to another, interactively.  Why another one of these?

I didn't like the information the others provide.  I like seeing who last touched a branch and when!

![#](https://bitbucket.org/zeg-io/git-flip/raw/master/ignore-assets/demo.gif)

## Installation

```sh
npm i -g git-flip
```

To integrate as an alias within git proper:

```sh
# add a change branch alias
git config --global alias.cb '!git-flip'

# or set it to flip
git config --global alias.flip '!git-flip'

# and then realize that typing "flip" is a lot of work and shorten it more
git config --global alias.f '!git-flip'
```

## Usage

To select a new branch you can either call `git-flip` or the short-form alias `gf`. Alternately you can integrate with git as an aias (see above).

```sh
$ git-flip
```

```sh
$ gf
```
## Under the covers
It runs straight git commands. This allows all the hard work the git team already put into getting things right to be used rather than a custom library for it.

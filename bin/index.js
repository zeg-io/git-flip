#!/usr/bin/env node
const execa = require('execa')
const { Select } = require('enquirer')
const dayjs = require('dayjs')
const dedupe = require('dedupe')
const AdvancedFormat = require('dayjs/plugin/advancedFormat')
const cmd = require('../lib/command-parser')
const { parseBranch, formatBranchString } = require('../lib/format-helper')
const { sortByEditDate } = require('../lib/sort-branch')

const execaOptions = {
  stderr: 'inherit'
}

dayjs.extend(AdvancedFormat)

const main = async () => {
  try {
    let branchData = await execa(...cmd('git --no-pager branch --all'))

    branchData = branchData.stdout

    const choicesWithDupes = branchData
      .split('\n')
      .map(rawBranch => {
        const branch = rawBranch.match(/[^ ]+$/)[0]

        if (rawBranch.includes(' -> ')) return false
        return {
          current: rawBranch.substr(0, 1) === '*',
          remote: rawBranch.includes('remotes/'),
          fullBranch: branch,
          branch: branch.replace('remotes/origin/', '')
        }
      })
      .filter(nuke => nuke)
    const choices = dedupe(choicesWithDupes, choice => choice.branch)
    const pArray = choices.map(b => {
      const firstLine = execa.sync(
        'git',
        ['show', '--format="%aI %ar by %an"', b.fullBranch],
        execaOptions
      ).stdout

      return parseBranch(b, firstLine)
    })
    return Promise.all(pArray).then(async branches => {
      const formattedChoices = branches
        .sort(sortByEditDate)
        .map(formatBranchString)

      const prompt = new Select({
        name: 'template',
        message: 'Select a branch',
        choices: formattedChoices.concat({
          name: '> cancel <',
          message: 'Control+C to quit.'
        })
      })

      try {
        const choice = await prompt.run()

        if (!choice || choice === '> cancel <') return false

        return execa(
          ...cmd(
            `git checkout ${choice}`,
            Object.assign({}, execaOptions, { stdout: 'inherit' })
          )
        ).catch(err => {})
      } catch (err) {
        console.error(err)
      }
    })
  } catch (err) {
    if (err.stderr.includes('not a git repository'))
      console.error(
        'Error: git-flip must be run in a directory which is a git repository.'
      )
    else console.error('Error', err)
  }
}

main()

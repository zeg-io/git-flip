module.exports = (commandString, execaOptions = {}) => {
  const commandArray = commandString.split(' ')

  const command = commandArray.splice(0, 1)[0]
  return [command, commandArray, execaOptions]
}

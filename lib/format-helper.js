const chalk = require('chalk')
const dayjs = require('dayjs')

const localBranchIcon = '⦿'
const remoteBranchIcon = '○'
const gitDateMatch = /^([\d|-]*T[\d|:|\-|+]*) (.*) by (.*)$/

const formatBranchName = branchObject => {
  // remotes/origin/fix-date-error
  if (branchObject.branch.includes('remotes/')) {
    branchObject.branch = branchObject.branch.substring(8)
  }
  return branchObject
}

const parseBranch = (branch, firstLine) => {
  if (firstLine) {
    let edits = firstLine.split('\n')[0].replace(/"/g, '')
    const matches = edits.match(gitDateMatch)
    const [raw, editDate, elapsed, editorName] = matches

    return { ...formatBranchName(branch), editDate, elapsed, editorName }
  }
  return branch
}

const formatBranchString = b => {
  const thisDay = dayjs(b.editDate).isAfter(dayjs().format('MM-DD-YYYY'))
  const thisWeek = dayjs(b.editDate).isAfter(dayjs().subtract(1, 'week'))
  const icon = b.remote ? remoteBranchIcon : localBranchIcon

  let thisBranch = `${icon} ${b.branch.padEnd(20, ' ')}`

  thisBranch = b.remote ? chalk.italic.red(thisBranch) : thisBranch

  thisBranch = b.current
    ? chalk.yellow.bold(thisBranch)
    : chalk.yellow(thisBranch)

  let dateFormat = 'MMM Do @ HH:mm - YYYY'
  if (thisWeek) dateFormat = 'ddd @ hh:mm A'
  if (thisDay) dateFormat = 'h:mm:ss A'

  let elapsedString = `${b.elapsed} —`
  if (!thisDay && thisWeek) elapsedString = 'on'

  return {
    name: b.branch,
    message: `${thisBranch} — ${chalk.cyan(
      b.editorName
    )}: ${elapsedString} ${chalk.italic(dayjs(b.editDate).format(dateFormat))}`
  }
}

module.exports = {
  parseBranch,
  formatBranchString
}

module.exports.sortByEditDate = (a, b) => {
  if (a.editDate < b.editDate) return 1
  if (a.editDate > b.editDate) return -1
  return 0
}
